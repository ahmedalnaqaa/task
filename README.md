Task API Service
=======

Installation commands
=======
* `composer insall`
* `php bin/console doctrine:database:create`
* `php bin/console doctrine:schema:update --force`
* `php bin/console assets:install`
* `php bin/console server:start`
* Go to http://127.0.0.1:8000